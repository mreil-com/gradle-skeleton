# gradle-skeleton

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

> Minimal multi-module project for [Gradle][gradle]


# Install

Clone the repository.

 
# Usage

Set project properties:

* In `build.gradle`
    * `group`
* In `gradle.properties`
    * `version`
    
`./gradlew build` builds the project.


# Changelog

See the Changelog [here][changelog].


# Maintainers

[Markus Reil][bb-mreil-com]


# Contributing

Feel free to dive in! [Open an issue][issues] or raise a [Pull Request][pr].


# License

[MIT][license]


[gradle]: https://gradle.org/
[bb-mreil-com]: https://bitbucket.org/mreil-com/
[pr]: ../../pull-requests/
[issues]: ../../issues/
[license]: LICENSE.txt
[changelog]: CHANGELOG.md
